# PAS Project Server :rocket:

## Introduction

Web app with face and speaker recognition !:fireworks:

## Project setup

- Install last npm release [here](https://nodejs.org/en/download/)
- Install MongoDb [here](https://docs.mongodb.com/manual/installation/)
- Install FFMPEG [here](https://web.archive.org/web/20200918193047/https://ffmpeg.zeranoe.com/builds/)
    - Unzip .zip folder to the folder you want
    - Add the path to the bin folder of ffmpeg in the PATH environment variable

## Steps:
Follow this instructions to fully install PAS project (front and back)

1- Download GIT repo Front & back master branch:

FRONTEND:
```
git clone https://gitlab.com/pas2/pas-project/-/tree/master
```
BACKEND : 
```
git clone https://gitlab.com/pas2/pas-project-server/-/tree/master
```

You have now one folder for front and another one for backend:

/FRONT

/BACK

2 - Environement setup - Anaconda
You need now the good environement to run the model and train it.
Lets Download anaconda here: https://www.anaconda.com/products/individual
Anaconda will help you to create different environment and have differents versions of python, tensorflow and more on the same PC.

After anaconda installation, open an anaconda prompt and go into BACK/websocket_server/ folder
You will find a file called environment.YML, you can use this file to create your anaconda setup to run the server and the model by doing the following cmd:
```
conda env create -f environment.yml
conda activate websocketServer
```
You are now into your environement called "websocketServer" and you have every packages to run the project

3 - Create .env into /BACK folder
Change pythondPath and wdPath variables:
- Change the path to your python directory (pythondPath) (python use in your websocketServer environment, do "where python" in anaconda cmd line to know the path)
- Change path of the current working directory (wdPath), which is YOURUSERDIRECTORY\BACK\websocket_server\training\

4 - Install dependencies:

Go to /BACK and /FRONT and run
```
npm install
```
5 - Into FRONT folder:

Start vue.js webapp with this command:
```
npm run serve
```

6 - Into BACK folder:
Start node.js server: 
```
npm start
```
7 - Into BACK/websocketServer/, start websocket server (into your anaconda env) with:
```
python server.py 
```

8 - Add people, train your model and let's go!

Go to "Database" tab and add people (max. 3), then train the model
After ~2min you can go into video/webcam tab and see if the model recognize you !!

## Contributors
- Augustin LE
- Alexis JAMBUT
- Gabriel FERNANDEZ CASA