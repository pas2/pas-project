import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#0088FF",
        secondary: "rgba(0, 136, 255, 0.07)",
        background: "rgba(0, 136, 255, 0.07)"
      },
      dark: {
        primary: "#0088FF",
        secondary: "rgba(0, 136, 255, 0.07)",
        background: "rgba(0, 136, 255, 0.07)"
      }
    }
  }
});
