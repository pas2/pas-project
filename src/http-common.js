import axios from "axios";

const modelHttp = axios.create({
  baseURL: "http://localhost:4500/api",
  headers: {
    "Content-type": "application/json"
  }
});

const trainModelHttp = axios.create({
  baseURL: "http://localhost:4500/trainapi",
  headers: {
    "Content-type": "application/json"
  }
});

const filesHttp = axios.create({
  baseURL: "http://localhost:4500/filesapi",
  headers: {
    "Content-type": "multipart/form-data"
  }
});

const filesHttp2 = axios.create({
  baseURL: "http://localhost:4500/filesapi",
  headers: {
    "Content-type": "application/json"
  }
});

export { modelHttp, trainModelHttp, filesHttp, filesHttp2 };
