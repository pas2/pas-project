import { filesHttp, filesHttp2 } from "../http-common";

class UploadDataServices {
  uploadImages(data) {
    return filesHttp.post("/uploadImages", data);
  }

  uploadAudios(data) {
    return filesHttp.post("/uploadAudios", data);
  }

  getFiles(data) {
    return filesHttp2.post("/getFiles", data);
  }
}

export default new UploadDataServices();
