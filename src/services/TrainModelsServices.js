import { trainModelHttp } from "../http-common";

class TrainModelsServices {
  trainModels(data) {
    return trainModelHttp.post("/trainModels", data);
  }
}

export default new TrainModelsServices();
