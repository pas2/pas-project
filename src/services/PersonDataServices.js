import { modelHttp } from "../http-common";

class PersonDataServices {
  getAll() {
    return modelHttp.get("/persons");
  }

  get(id) {
    return modelHttp.get(`/persons/${id}`);
  }

  create(data) {
    return modelHttp.post("/persons", data);
  }

  update(id, data) {
    return modelHttp.put(`/persons/${id}`, data);
  }

  delete(id) {
    return modelHttp.delete(`/persons/${id}`);
  }
}

export default new PersonDataServices();
