import Vue from "vue";
import VueRouter from "vue-router";
import Webcam from "../views/Webcam.vue";
import Video from "../views/Video.vue";
import Sound from "../views/Sound.vue";
import APIsTest from "../views/APIsTest.vue";
import Database from "../views/Database.vue";
import Person from "../views/Person.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Webcam recognition",
    component: Webcam
  },
  {
    path: "/video",
    name: "Video recognition",
    component: Video
  },
  {
    path: "/sound",
    name: "Sound recognition",
    component: Sound
  },
  {
    path: "/apisTest",
    name: "APIs Test",
    component: APIsTest
  },
  {
    path: "/database/persons",
    name: "Database",
    component: Database
  },
  {
    path: "/database/persons/:id",
    name: "Person",
    component: Person
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
